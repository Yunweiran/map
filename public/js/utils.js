
//判断是否为手机号码
function poneAvailable(phone) {
    var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
    if (!myreg.test(phone)) {
        return false;
    } else {
        return true;
    }
}
//提示信息
function alertFun(msg, flag) {
    if (flag) {
        ! function (win, $) {
            var dialog = win.YDUI.dialog;
            dialog.toast(msg, flag, 1500);
        }(window, jQuery, msg, flag);
    } else {
        ! function (win, $) {
            var dialog = win.YDUI.dialog;
            dialog.alert(msg);
        }(window, jQuery, msg);
    }

}

function loading(msg) {
    ! function (win, $) {
        var dialog = win.YDUI.dialog;
        dialog.loading.open(msg);
    }(window, jQuery, msg, loading);
}

function close() {
    ! function (win, $) {
        var dialog = win.YDUI.dialog;
        dialog.loading.close(); //移除loading
    }(window, jQuery);
}
