var express = require('express');
var router = express.Router();
var pool = require('../config/mysqlConfig');//引入数据库配置文件
var request = require('request');
const crypto = require('crypto');

var sign_key = "Jhy$xBs5aftk@tDS";
var api_key = "bY@g2fAGRQ$A4C4K";
var uid = "blmpublic";
var upwd = "52bb6b2f09d7095a1c1afec721345817";
var version = "0_1.1";



router.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST");
    res.header("X-Powered-By", ' 3.2.1');
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});


router.post('/', function (req, res, fields) {
    var cityStr = req.body.city;
    var cityList = Array.from(new Set(cityStr.split(',')));
    var key = '';
    cityList.forEach((element, index) => {
        if (index == cityList.length - 1) {
            key += 'stop:*' + element + '*';
        } else {
            key += 'stop:*' + element + '* OR ';
        }
    });
    var weightMin = parseInt(req.body.weightMin),
        weightMax = parseInt(req.body.weightMax);
    if (weightMin > 0) {
        weightMin = Number(weightMin);
    } else {
        weightMin = '';
    }
    if (weightMax > 0) {
        weightMax = Number(weightMax);
    } else {
        weightMax = '';
    }
    request.get(
        {
            url: 'http://116.62.87.217:8983/solr/cld/select',
            form: {
                q: key,
                "rows": "20000"
            },
            encoding: 'utf8'
        },
        function (error, response, body) {
            if (response.statusCode == 200) {
                var list = JSON.parse(body).response.docs;
                if (list.length > 0) {
                    var mmsis = [];
                    var mmsiStr = '';
                    list.forEach((element, index) => {
                        mmsis.push(element.mmsi);
                        if (index == list.length - 1) {
                            mmsiStr += element.mmsi;
                        } else {
                            mmsiStr += element.mmsi + ',';
                        }
                    });
                    if (weightMin && !weightMax) {
                        var sql = "SELECT au.id id, au.name name, au.mobile, s.id, s.no, s.tonnage, s.jiuweima  FROM ship  s INNER JOIN ship_user su ON su.ship_id = s.id INNER JOIN app_user au ON au.id = su.user_id  WHERE  s.tonnage >= " + weightMin + " AND s.jiuweima in (" + mmsiStr + ")";
                    } else if (!weightMin && weightMax) {
                        var sql = "SELECT au.id id, au.name name, au.mobile, s.id, s.no, s.tonnage, s.jiuweima  FROM ship  s INNER JOIN ship_user su ON su.ship_id = s.id INNER JOIN app_user au ON au.id = su.user_id  WHERE  s.tonnage <= " + weightMax + " AND s.jiuweima in (" + mmsiStr + ")";
                    } else if (!weightMin && !weightMax) {
                        var sql = "SELECT au.id id, au.name name, au.mobile, s.id, s.no, s.tonnage, s.jiuweima  FROM ship  s INNER JOIN ship_user su ON su.ship_id = s.id INNER JOIN app_user au ON au.id = su.user_id  WHERE  s.jiuweima in (" + mmsiStr + ")";
                    } else {
                        var sql = "SELECT au.id id, au.name name, au.mobile, s.id, s.no, s.tonnage, s.jiuweima  FROM ship  s INNER JOIN ship_user su ON su.ship_id = s.id INNER JOIN app_user au ON au.id = su.user_id  WHERE  s.tonnage >= " + weightMin + " AND s.tonnage <= " + weightMax + " AND s.jiuweima in (" + mmsiStr + ")";
                    }
                    pool.getConnection(function (err, conn) {
                        if (err) {
                            res.status(500).send(JSON.stringify({
                                status: 0,
                                message: '请求数据失败',
                                data: null
                            }));
                        } else {
                            conn.query(sql, function (qerr, vals, fields) {
                                if (qerr) {
                                    res.status(500).send(JSON.stringify({
                                        status: 0,
                                        message: '请求失败',
                                        data: null
                                    }));
                                }
                                conn.release();
                                data(mmsis, vals, res);
                            });
                        }
                    });



                } else {
                    return res.send(JSON.stringify({
                        status: 1,
                        message: '请求成功',
                        data: []
                    }));
                }
            } else {
                return res.send(JSON.stringify({
                    status: 0,
                    message: '数据请求失败',
                    data: null
                }));
            }
        }
    );
});



function data(mmsis, vals, res) {
    if (mmsis != null) {
        var groupedArray = group(mmsis, 300);
        var requsetlist = [];//请求地址数组
        var newarr = [];
        groupedArray.forEach((element) => {
            var str = '';
            if (element != null && element.length > 0) {
                for (var i = 0; i < element.length; ++i) {
                    if (i == element.length - 1) {
                        str += element[i];
                    } else {
                        str += element[i] + "|";
                    }
                }
            }
            var basePath = "http://shippingcgi.boloomo.com/blmcgi?" + getRequestURL("0x5102", new Buffer("{mmsis:\"" + str + "\"}").toString('base64'));
            requsetlist.push(basePath);

        });
        Promise.all(requsetlist.map(function (url) {
            return new Promise(function (resolve, reject) {
                request(url, function (err, resopnse, body) {
                    resolve(body)
                })
            })
        })).then(function (results) {
            results.forEach(element => {
                eval('(' + element + ')').forEach(item => {
                    newarr.push(item)
                });
            });
            if (vals) {
                vals.forEach(item_1 => {
                    newarr.forEach(item_2 => {
                        if (item_1.jiuweima == item_2.mmsi) {
                            item_1.longitude = item_2.x;
                            item_1.latitude = item_2.y;
                            item_1.speed = item_2.sog / 10;
                            item_1.ptime = timeStampToDate(item_2.time);
                        }
                    });
                });
                return res.send(JSON.stringify({
                    status: 1,
                    message: '请求成功',
                    data: vals
                }));
            } else {
                return res.send([]);
            }

        }).catch(error => {
            return res.send(JSON.stringify({
                status: 0,
                message: '请求数据失败',
                data: null
            }));
        })
    }
}

function timeStampToDate(timestamp) {
    var date = new Date(timestamp * 1000);
    const Y = date.getFullYear() + '-';
    const M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    const D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
    const h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    const m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
    const s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    return (Y + M + D + h + m + s);
}

// 获取请求参数
function getRequestURL(cmd, param) {
    var sign = getSign(cmd, param);
    var sb = "sign=" + sign + "&api_key=" + "yAjFdLc$Mb76@9rC" + "&cmd=" + cmd + "&param=" + param + "&time=" + new Date().getTime();

    return sb.toString();
}
function getSign(cmd, param) {
    var sb = "C#t36KclJs6JlT$y" + "api_key" + "yAjFdLc$Mb76@9rC" + "cmd" + cmd + "param" + param;
    try {
        return encryptSHA(sb.toString());
    } catch (e) {
    }
    return null;
}

// 按照传入长度切割数组
function group(array, subGroupLength) {
    var index = 0;
    var newArray = [];
    while (index < array.length) {
        newArray.push(array.slice(index, index += subGroupLength));
    }
    return newArray;
}
//请求参数加密
function encryptSHA(data) {
    const hash = crypto.createHash('sha1');
    hash.update(data);
    var bytes = hash.digest('hex');
    return bytes;
}
module.exports = router;

